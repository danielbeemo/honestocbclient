package com.honestfund.ocb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml", inheritLocations = false)
public class OCBClientImplTest {
//  private static Logger log = LoggerFactory.getLogger(OCBClientImplTest.class);
  private static int remainedPoint = 0;
  private static int usePoint = 0;
  private static int accumulatePoint = 0;
  private static String tx_no = null;
  @Autowired private OCBClient client = null;
  
  @Before
  public void setUp(){
    usePoint = 1;
    accumulatePoint = 1;
  }
  
  @Test
  public void test1CheckPoint() throws Exception {
    String cid = "WkNNW7jwUglpbLC0u+Iq3oxhIWLeeL20ZIdqvSH9eANHuvfBl/xloscuR2iVSeOP9MNz/Ugwdwhg3IHh0gvksg==";
    String password = "ok1357";
    Map<String, Object> point = client.checkPoint(cid, password);
    assertNotNull(point);
    remainedPoint = (int)point.get("point");
    tx_no = (String) point.get("tx_no");
  } 
  
  //@Test(expected=AuthenticationException.class)
  public void testCheckPointException() throws Exception {
    String cid = "WkNNW7jwUglpbLC0u+Iq3oxhIWLeeL20ZIdqvSH9eANHuvfBl/xloscuR2iVSeOP9MNz/Ugwdwhg3IHh0gvksg==";
    String password = "ok1357a";
    Map<String, Object> point = client.checkPoint(cid, password);
    assertNotNull(point);
    remainedPoint = (int)point.get("point");
  } 

  //@Test
  public void testUsePoint() throws Exception {
    String cid = "WkNNW7jwUglpbLC0u+Iq3oxhIWLeeL20ZIdqvSH9eANHuvfBl/xloscuR2iVSeOP9MNz/Ugwdwhg3IHh0gvksg==";
    String password = "ok1357";
    int point = client.usePoint(cid, tx_no, password, usePoint);
    assertNotNull(point);
    assertEquals(point, remainedPoint - usePoint);
  }
  
  @Test
  public void test2AccumulatePoint() throws Exception {
    String cid = "WkNNW7jwUglpbLC0u+Iq3oxhIWLeeL20ZIdqvSH9eANHuvfBl/xloscuR2iVSeOP9MNz/Ugwdwhg3IHh0gvksg==";
    int point = client.accumulatePoint(cid, tx_no, accumulatePoint);
    assertNotNull(point);
    assertEquals(point, remainedPoint + accumulatePoint);
  }

  //@Test
  public void testCancelPoint() throws Exception {
    String cid = "WkNNW7jwUglpbLC0u+Iq3oxhIWLeeL20ZIdqvSH9eANHuvfBl/xloscuR2iVSeOP9MNz/Ugwdwhg3IHh0gvksg==";
    int point = client.cancelPoint(cid, tx_no, usePoint);
    assertNotNull(point);
    assertEquals(point, remainedPoint);
  }
  
  //@Test
  public void testCheckResultPoint() throws Exception {
    String cid = "WkNNW7jwUglpbLC0u+Iq3oxhIWLeeL20ZIdqvSH9eANHuvfBl/xloscuR2iVSeOP9MNz/Ugwdwhg3IHh0gvksg==";
    int point = client.checkResultPoint(cid, tx_no);
    assertNotNull(point);
    assertEquals(point, usePoint);
  }
}
