package com.honestfund.ocb;

import static org.junit.Assert.*;

import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext-test.xml", inheritLocations = false)
public class BasicTest {

	@Resource(name="ocbClient")
	private OCBClient ocbClient;
	
	@Test
	public void testBasic() throws Exception{

		String cid = "WkNNW7jwUglpbLC0u+Iq3oxhIWLeeL20ZIdqvSH9eANHuvfBl/xloscuR2iVSeOP9MNz/Ugwdwhg3IHh0gvksg==";
		String password = "ok1357";
		Map<String, Object> originalAvPointMap = ocbClient.checkPoint(cid, password);

		String tx_no = (String) originalAvPointMap.get("tx_no");
		//같은금액을 여러번 승인하면 에러가 나는것 같다. -_-
		int usePoint = new Random().nextInt(100);
		int remainAvPoint = ocbClient.usePoint(cid, tx_no, password, usePoint);
		assertEquals(remainAvPoint, ((Integer) originalAvPointMap.get("point")) - usePoint);
		
		System.out.println("originPoint=" + originalAvPointMap);
		System.out.println("usedPoint=" + usePoint);
		System.out.println("remainPoint=" + remainAvPoint);
	}
}
