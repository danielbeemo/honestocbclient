package com.honestfund.ocb.dao;

import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.honestfund.ocb.OCBDataRepository;

@Repository("ocbDao")
public class OCBTestDao implements OCBDataRepository {
  private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

  public OCBTestDao(@Autowired DataSource ds) {
    this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(ds);
  }

  @Override
  public Map<String, Object> selectOCBAuth(String cid, String tx_no) {
    Map<String, Object> info = namedParameterJdbcTemplate.queryForMap(
        "SELECT auth_id, mct_tr_no, mct_tr_date, tx_no, registered_date FROM e_ocb_auth WHERE auth_id = :cid and tx_no = :tx_no",
        new MapSqlParameterSource("cid", cid).addValue("tx_no", tx_no));
    return info;
  }
  
  @Override
  public int insertOrUpdateOCBAuth(Map<String, Object> params) {
    return namedParameterJdbcTemplate.update(
        "INSERT INTO e_ocb_auth(auth_id, mct_tr_no, mct_tr_date, tx_no, registered_date) VALUES "
            + "(:auth_id, :mct_tr_no, :mct_tr_date, :tx_no, NOW())",
        new MapSqlParameterSource(params));
  }
}
