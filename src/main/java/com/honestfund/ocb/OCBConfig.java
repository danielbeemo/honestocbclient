package com.honestfund.ocb;

public class OCBConfig {
	
  private String server;
	//OCB 와의 통신을 위한 honestfund 의 정보.
	private String NxMctNo; //가맹점 정보
	private String SubMallId; //제휴사 코드
	private String SubMallName; //가맹점
	private String SubMallCorpNo;
	private String cipherKey;
	private String MxPassKey;
	private String MctId;
	private String PrctName;
	private String paymesTpCd;

	public String getNxMctNo() {
		return NxMctNo;
	}
	public void setNxMctNo(String nxMctNo) {
		NxMctNo = nxMctNo;
	}
	public String getSubMallId() {
		return SubMallId;
	}
	public void setSubMallId(String subMallId) {
		SubMallId = subMallId;
	}
	public String getSubMallName() {
		return SubMallName;
	}
	public void setSubMallName(String subMallName) {
		SubMallName = subMallName;
	}
	public String getSubMallCorpNo() {
		return SubMallCorpNo;
	}
	public void setSubMallCorpNo(String subMallCorpNo) {
		SubMallCorpNo = subMallCorpNo;
	}
	public String getCipherKey() {
    return cipherKey;
  }
  public void setCipherKey(String cipherKey) {
    this.cipherKey = cipherKey;
  }
  public String getMxPassKey() {
		return MxPassKey;
	}
	public void setMxPassKey(String mxPassKey) {
		MxPassKey = mxPassKey;
	}
	public String getMctId() {
		return MctId;
	}
	public void setMctId(String mctId) {
		MctId = mctId;
	}
	public String getPrctName() {
		return PrctName;
	}
	public void setPrctName(String prctName) {
		PrctName = prctName;
	}
	public String getPaymesTpCd() {
		return paymesTpCd;
	}
	public void setPaymesTpCd(String paymesTpCd) {
		this.paymesTpCd = paymesTpCd;
	}
  public String getServer() {
    return this.server;
  }
  public void setServer(String server) {
    this.server = server;
  }
  
}
