package com.honestfund.ocb;

import java.util.Map;

public interface OCBClient {

	Map<String, Object> checkPoint(String cid, String password) throws Exception;
	int usePoint(String cid, String tx_no, String password, int point) throws Exception;
	int accumulatePoint(String cid, String tx_no, int point) throws Exception;
	int cancelPoint(String cid, String tx_no, int point) throws Exception;
	int checkResultPoint(String cid, String tx_no) throws Exception;

}
