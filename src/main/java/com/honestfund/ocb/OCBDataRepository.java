package com.honestfund.ocb;

import java.util.Map;

public interface OCBDataRepository {

  Map<String, Object> selectOCBAuth(String cid, String tx_no);

  int insertOrUpdateOCBAuth(Map<String, Object> params);

}
