package com.honestfund.ocb;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.util.StringUtils;

public class OCBClientImpl implements OCBClient{

	private PoolingHttpClientConnectionManager cm;

	private OCBConfig ocbConfig;
	private OCBPayloadHandler payloadHandler;

	private OCBDataRepository dr;
	
	public OCBClientImpl(OCBConfig ocbConfig, OCBDataRepository dr) {
		System.setProperty("jsse.enableSNIExtension", "false");
		
		payloadHandler = new OCBPayloadHandler(ocbConfig);
		this.ocbConfig = ocbConfig;
	
		cm = new PoolingHttpClientConnectionManager();
		cm.setMaxTotal(20);
		cm.setDefaultMaxPerRoute(3);
		this.dr = dr;
	}
	
	@Override
	public Map<String, Object> checkPoint(String cid, String password) throws Exception {

		String mctTrNo = String.valueOf(System.currentTimeMillis());
		String mctTrDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
		
		CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(cm).build();
		List <NameValuePair> nvps = payloadHandler.buildForCheck(cid, password, mctTrNo, mctTrDate);
		HttpPost httpPost = new HttpPost(ocbConfig.getServer() + "/dlp/api/point/auth");
		httpPost.setEntity(new UrlEncodedFormEntity(nvps));
		
		CloseableHttpResponse response = httpClient.execute(httpPost);
		if(response.getStatusLine().getStatusCode() != 200){
			throw new IllegalStateException("fail to execute auth request. auth_id=" + cid);
		}
		
		Map<String, Object> resMap = payloadHandler.parseResponse(response);
		String replyCode = (String)resMap.get("ReplyCode");
		String replyMessage = (String)resMap.get("ReplyMessage");
		
		if("INT4019".equals(replyCode) || "INT4022".equals(replyCode) || 
		   "INT4041".equals(replyCode) || "INT4042".equals(replyCode) ||
		   "INT4043".equals(replyCode)){ // 비밀번호 틀림 또는 3회이상 틀림 또는 자릿수틀림 등
		  throw new AuthenticationException(replyMessage);
		}else if(!"000000".equals(replyCode)){
			throw new IllegalStateException("fail to execute auth request. replyCode=" + replyCode + ", replyMessage=" + replyMessage + ", auth_id=" + cid);
		}
		String avPointStr = (String)resMap.get("AvPoint");
		int avPoint = Integer.parseInt(avPointStr);
		String txNo = (String)resMap.get("TxNo");

		Map<String, Object> params = new HashMap<>();
		params.put("auth_id", cid);
		params.put("mct_tr_no", mctTrNo);
		params.put("mct_tr_date", mctTrDate);
		params.put("tx_no", txNo);
		params.put("point", avPoint);
		int affected = dr.insertOrUpdateOCBAuth(params);
		if(affected != 1 && affected != 2){
			throw new IllegalStateException("fail to insert or update ocb auth. affectedRow = " + affected + ", auth_id=" + cid);
		}
		
		return params;
	}

	@Override
	public int usePoint(String cid, String tx_no, String password, int point) throws Exception{
		
		Map<String, Object> ocbAuth = dr.selectOCBAuth(cid, tx_no);
		if(ocbAuth == null || StringUtils.isEmpty(ocbAuth.get("mct_tr_no"))){
			throw new IllegalStateException("not found ocb auth. auth_id=" + cid);
		}
		String mctTrNo = (String)ocbAuth.get("mct_tr_no");
		String mctTrDate = (String)ocbAuth.get("mct_tr_date");
	
		CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(cm).build();
		List <NameValuePair> nvps = payloadHandler.buildForUse(cid, password, point, mctTrNo, mctTrDate, tx_no);
		HttpPost httpPost = new HttpPost(ocbConfig.getServer() + "/dlp/api/point/approval");
		httpPost.setEntity(new UrlEncodedFormEntity(nvps));
		
		CloseableHttpResponse response = httpClient.execute(httpPost);
		int statusCode = response.getStatusLine().getStatusCode();
		if(statusCode != 200){
			throw new IllegalStateException("fail to execute auth request. status_code=" + statusCode + ", auth_id=" + cid);
		}
		
		Map<String, Object> resMap = payloadHandler.parseResponse(response);
		String replyCode = (String)resMap.get("ReplyCode");
		if(!"000000".equals(replyCode)){
			throw new IllegalStateException("fail to execute auth request. replyCode=" + replyCode + ", auth_id=" + cid);
		}
		
		return Integer.parseInt((String)resMap.get("AvPoint"));
	}

	@Override
	public int accumulatePoint(String cid, String tx_no, int point) throws Exception {
		Map<String, Object> ocbAuth = dr.selectOCBAuth(cid, tx_no);
		if(ocbAuth == null || StringUtils.isEmpty(ocbAuth.get("mct_tr_no"))){
			throw new IllegalStateException("not found ocb auth. auth_id=" + cid);
		}
		String mctTrNo = (String)ocbAuth.get("mct_tr_no");
		String mctTrDate = (String)ocbAuth.get("mct_tr_date");
	
		CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(cm).build();
		List <NameValuePair> nvps = payloadHandler.buildForAccumulate(cid, point, mctTrNo, mctTrDate);
		HttpPost httpPost = new HttpPost(ocbConfig.getServer() + "/dlp/api/point/approval");
		httpPost.setEntity(new UrlEncodedFormEntity(nvps));
		
		CloseableHttpResponse response = httpClient.execute(httpPost);
		int statusCode = response.getStatusLine().getStatusCode();
		if(statusCode != 200){
			throw new IllegalStateException("fail to execute auth request. status_code=" + statusCode + ", auth_id=" + cid);
		}
		
		Map<String, Object> resMap = payloadHandler.parseResponse(response);
		String replyCode = (String)resMap.get("ReplyCode");
		if(!"000000".equals(replyCode)){
			throw new IllegalStateException("fail to execute auth request. replyCode=" + replyCode + ", auth_id=" + cid);
		}
		
		return Integer.parseInt((String)resMap.get("AvPoint"));
	}
	
	@Override
	public int cancelPoint(String cid, String tx_no, int point) throws Exception{
	  
	  Map<String, Object> ocbAuth = dr.selectOCBAuth(cid, tx_no);
	  if(ocbAuth == null || StringUtils.isEmpty(ocbAuth.get("mct_tr_no"))){
	    throw new IllegalStateException("not found ocb auth. auth_id=" + cid);
	  }
	  String mctTrNo = (String)ocbAuth.get("mct_tr_no");
	  String mctTrDate = (String)ocbAuth.get("mct_tr_date");
	  
	  CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(cm).build();
	  List <NameValuePair> nvps = payloadHandler.buildForCancel(cid, point, mctTrNo, mctTrDate, tx_no);
	  HttpPost httpPost = new HttpPost(ocbConfig.getServer() + "/dlp/api/point/cancel");
	  httpPost.setEntity(new UrlEncodedFormEntity(nvps));
	  
	  CloseableHttpResponse response = httpClient.execute(httpPost);
	  int statusCode = response.getStatusLine().getStatusCode();
	  if(statusCode != 200){
	    throw new IllegalStateException("fail to execute auth request. status_code=" + statusCode + ", auth_id=" + cid);
	  }
	  
	  Map<String, Object> resMap = payloadHandler.parseResponse(response);
	  String replyCode = (String)resMap.get("ReplyCode");
	  if(!"000000".equals(replyCode)){
	    throw new IllegalStateException("fail to execute auth request. replyCode=" + replyCode + ", auth_id=" + cid);
	  }
	  String avPointStr = (String)resMap.get("AvPoint");
	  int avPoint = Integer.parseInt(avPointStr);
	  return avPoint;
	}

	@Override
	public int checkResultPoint(String cid, String tx_no) throws Exception{
	  
	  Map<String, Object> ocbAuth = dr.selectOCBAuth(cid, tx_no);
	  if(ocbAuth == null || StringUtils.isEmpty(ocbAuth.get("mct_tr_no"))){
	    throw new IllegalStateException("not found ocb auth. auth_id=" + cid);
	  }
	  String mctTrNo = (String)ocbAuth.get("mct_tr_no");
	  String mctTrDate = (String)ocbAuth.get("mct_tr_date");
	  
	  CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(cm).build();
	  List <NameValuePair> nvps = payloadHandler.buildForCheckResult(mctTrNo, mctTrDate, tx_no);
	  HttpPost httpPost = new HttpPost(ocbConfig.getServer() + "/dlp/api/point/inquiryResult");
	  httpPost.setEntity(new UrlEncodedFormEntity(nvps));
	  
	  CloseableHttpResponse response = httpClient.execute(httpPost);
	  int statusCode = response.getStatusLine().getStatusCode();
	  if(statusCode != 200){
	    throw new IllegalStateException("fail to execute auth request. status_code=" + statusCode + ", auth_id=" + cid);
	  }
	  
	  Map<String, Object> resMap = payloadHandler.parseResponse(response);
	  String replyCode = (String)resMap.get("ReplyCode");
	  if(!"000000".equals(replyCode)){
	    throw new IllegalStateException("fail to execute auth request. replyCode=" + replyCode + ", auth_id=" + cid);
	  }
	  String avPointStr = (String)resMap.get("ResultPoint");
	  int avPoint = Integer.parseInt(avPointStr);
	  return avPoint;
	}

}
