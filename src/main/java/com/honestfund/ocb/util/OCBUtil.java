package com.honestfund.ocb.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.cnt.crypto.SeedCrypt;
import com.cnt.crypto.Engine.Seed;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class OCBUtil {

	public static Map<String, Object> jsonToMap(String json) {
		Map<String, Object> ret = new Gson().fromJson(json, new TypeToken<HashMap<String, Object>>() {}.getType());
		return ret;
	}
	
	public static String encrypt(String data, String cipherKey) {
    String _mode = Seed.MODE_ECB;//Seed.MODE_CBC;//Seed.MODE_CFB;//Seed.MODE_OFB
    String _pad = Seed.PAD_X923;//Seed.PAD_PKCS;//Seed.PAD_NO(사용하지 말것)
    
    SeedCrypt encrypter = new SeedCrypt();  //암호화용
    encrypter.init(encrypter.ENCRYPT_MODE, _mode, _pad, cipherKey.getBytes());
    return encrypter.encrypt(data);
  }

  public static NameValuePair kv(String k, String v) {
    return new BasicNameValuePair(k, v);
  }
}
