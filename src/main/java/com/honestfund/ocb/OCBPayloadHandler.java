package com.honestfund.ocb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cnt.util.ByteUtil;
import com.honestfund.ocb.util.OCBUtil;

public class OCBPayloadHandler {
  private static Logger log = LoggerFactory.getLogger(OCBPayloadHandler.class);
  
	private OCBConfig ocbConfig;
	
	public OCBPayloadHandler(OCBConfig ocbConfig){
		this.ocbConfig = ocbConfig;
	}
	
	/**
	 * 포인트 조회 파라미터
	 */
	public List<NameValuePair> buildForCheck(String cid, String password, String mctTrNo, String mctTrDate) throws Exception{
	  String amount = "10";
	  StringBuilder enc = new StringBuilder();
		enc.append("Amount=").append(amount).append(";").append("AuthId=").append(cid).append(";")
		  .append("AuthPwd=").append(password);
		
		List<NameValuePair> p = createCommonParam();
		p.add(OCBUtil.kv("AuthToken", authToken(mctTrNo, mctTrDate, amount)));
		p.add(OCBUtil.kv("MctTrNo", mctTrNo));
		p.add(OCBUtil.kv("MctTrDate", mctTrDate));
		p.add(OCBUtil.kv("Enc", OCBUtil.encrypt(enc.toString(), ocbConfig.getCipherKey())));
		return p;
	}

	/** 
	 * 포인트 사용 파라미터
	 */
	public List<NameValuePair> buildForUse(String cid, String password, int point, String mctTrNo, String mctTrDate, String txNo) throws Exception {
		String amount = "0";
		    
	  StringBuilder enc = new StringBuilder();
		enc.append("Amount=").append(amount).append(";").append("Point=").append(point).append(";")
		  .append("AuthId=").append(cid).append(";").append("AuthPwd=").append(password);
		
		List<NameValuePair> p = createCommonParam();
		p.add(OCBUtil.kv("AuthToken", authToken(mctTrNo, mctTrDate, amount)));
		p.add(OCBUtil.kv("MctTrNo", mctTrNo));
		p.add(OCBUtil.kv("MctTrDate", mctTrDate));
		p.add(OCBUtil.kv("TxNo", txNo));
		p.add(OCBUtil.kv("TxDivCd", "40"));
		p.add(OCBUtil.kv("StmtCd", "11"));
		p.add(OCBUtil.kv("ContDivCd", "08"));
		p.add(OCBUtil.kv("Smode", "N010"));
		p.add(OCBUtil.kv("Enc", OCBUtil.encrypt(enc.toString(), ocbConfig.getCipherKey())));
		
		return p;
	}
	
	
	/** 
	 * 포인트 적립 파라미터
	 */
	public List<NameValuePair> buildForAccumulate(String cid, int point, String mctTrNo, String mctTrDate) throws Exception {
	  String amount = "0";
	  String password = "";
		    
	  StringBuilder enc = new StringBuilder();
		enc.append("Amount=").append(amount).append(";").append("Point=").append(point).append(";")
		  .append("AuthId=").append(cid).append(";").append("AuthPwd=").append(password);
		
		List<NameValuePair> p = createCommonParam();
		p.add(OCBUtil.kv("AuthToken", authToken(mctTrNo, mctTrDate, amount)));
		p.add(OCBUtil.kv("MctTrNo", mctTrNo));
		p.add(OCBUtil.kv("MctTrDate", mctTrDate));
		p.add(OCBUtil.kv("TxNo", ""));
		p.add(OCBUtil.kv("TxDivCd", "22"));
		p.add(OCBUtil.kv("StmtCd", "01"));
		p.add(OCBUtil.kv("ContDivCd", "01"));
		p.add(OCBUtil.kv("Smode", "N011"));
		p.add(OCBUtil.kv("Enc", OCBUtil.encrypt(enc.toString(), ocbConfig.getCipherKey())));
		
		return p;
	}

	 /** 
   * 포인트 사용 취소 파라미터
   */
  public List<NameValuePair> buildForCancel(String cid, int point, String mctTrNo, String mctTrDate, String txNo) throws Exception {
    StringBuilder enc = new StringBuilder();
    enc.append("Point=").append(point).append(";").append("AuthId=").append(cid).append(";");
    
    List<NameValuePair> p = createCommonParam();
    p.add(OCBUtil.kv("AuthToken", authToken(mctTrNo, mctTrDate, "")));
    p.add(OCBUtil.kv("MctTrNo", mctTrNo));
    p.add(OCBUtil.kv("MctTrDate", mctTrDate));
    p.add(OCBUtil.kv("TxNo", txNo));
    p.add(OCBUtil.kv("TxDivCd", "42"));
    p.add(OCBUtil.kv("StmtCd", "22"));
    p.add(OCBUtil.kv("Enc", OCBUtil.encrypt(enc.toString(), ocbConfig.getCipherKey())));
    
    return p;
  }
  
  /**
   * 거래결과 조회용 파라미터 빌드
   * @return
   */
  public List<NameValuePair> buildForCheckResult(String mctTrNo, String mctTrDate, String txNo) {
    List<NameValuePair> p = createCommonParam();
    p.add(OCBUtil.kv("MctTrNo", mctTrNo));
    p.add(OCBUtil.kv("MctTrDate", mctTrDate));
    p.add(OCBUtil.kv("TxNo", txNo));
    return p;
  }

  private List<NameValuePair> createCommonParam() {
    List<NameValuePair> p = new ArrayList<>();
    p.add(OCBUtil.kv("NxMctNo", ocbConfig.getNxMctNo()));
    p.add(OCBUtil.kv("SubMallId", ocbConfig.getSubMallId()));
    p.add(OCBUtil.kv("SubMallName", ocbConfig.getSubMallName()));
    p.add(OCBUtil.kv("SubMallCorpNo", ocbConfig.getSubMallCorpNo()));
    p.add(OCBUtil.kv("PrctName", ocbConfig.getPrctName()));
    p.add(OCBUtil.kv("MctId", ocbConfig.getMctId()));
    p.add(OCBUtil.kv("paymesTpCd", ocbConfig.getPaymesTpCd()));
    return p;
  }
  
  
	public Map<String, Object> parseResponse(CloseableHttpResponse response) throws IOException {
	  Map<String, Object> resMap = null;
	  try(
	      InputStream is = response.getEntity().getContent();
	      BufferedReader in = new BufferedReader(new InputStreamReader(is, "UTF-8"));
	  ){
  		String inputLine;
  		StringBuilder sb = new StringBuilder();
  		while ((inputLine = in.readLine()) != null) {
  			sb.append(inputLine);
  		}
  		log.debug("json : " + sb.toString());
  		resMap = OCBUtil.jsonToMap(sb.toString());
	  }
		return resMap;
	}
	
  private String authToken(String mctTrNo, String mctTrDate, String amount) throws Exception {
    StringBuilder authTokenSB = new StringBuilder();
    authTokenSB.append(ocbConfig.getMxPassKey()).append(mctTrNo).append(mctTrDate).append(amount);  
    return ByteUtil.getEncMD5(authTokenSB.toString());
  }

}
